package utils

import (
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"gitlab.com/oleg.kuleba/goProtobuf/models"
	"github.com/golang/protobuf/proto"
)

// Флаги, использующиеся для валидации
const FileName string = "phoneNumbers.txt"
const PhoneFlag string = "phone"
const NameFlag string = "name"
const CityFlag string = "city"
const StreetFlag string = "street"
const BuildingOrApartmentFlag string = "buildingOrApartment"

var PhoneRegexp *regexp.Regexp = regexp.MustCompile("^[+]380([0-9]{9})$")
var NameRegexp *regexp.Regexp = regexp.MustCompile("^[0-9A-Za-z]{3,15}$")
var CityRegexp *regexp.Regexp = regexp.MustCompile("^[A-Za-z]{3,15}$")
var StreetRegexp *regexp.Regexp = regexp.MustCompile("^[0-9A-Za-z]{4,20}$")
var BuildingOrApartmentRegexp *regexp.Regexp = regexp.MustCompile("^[0-9A-Za-z]{1,5}$")

func AddContact(contact *models.Contact) bool {
	if _, err := os.Stat(FileName); os.IsNotExist(err) {
		_, err := os.Create(FileName)
		Check(err)
	}

	// Считываем данные из файла для проверки уникальности номера
	tmpF, err := ioutil.ReadFile(FileName)
	Check(err)
	book := &models.ContactBook{}
	err = proto.Unmarshal(tmpF, book)
	Check(err)

	// Перебираем все записи книги
	for _, item := range book.Contacts {
		// Если № совпадает (т.е. уже есть в файле), то сообщаем об этом и выходим ничего не меняя
		if item.Phone == contact.Phone {
			fmt.Println("Запись с таким номером уже существует. Для изменения данных используйте команду editContact")
			return false
		}
	}
	book.Contacts = append(book.Contacts, contact)

	out, err := proto.Marshal(book)
	Check(err)
	// Записываем в файл
	ioutil.WriteFile(FileName, out, 0644)
	Check(err)
	return true
}

func FindAll() {
	f, err := ioutil.ReadFile(FileName)
	Check(err)

	// Если файл пустой - выходим
	if IsFileEmpty(len(f)) {
		return
	}
	fmt.Println("Найдены записи:")
	contacts := &models.ContactBook{}
	err = proto.Unmarshal(f, contacts)
	//fmt.Println(contacts)
	// Отображаем контакты через форматированный вывод
	PrintContacts(contacts)
}

func FindByNumber(number string) {
	f, err := ioutil.ReadFile(FileName)
	Check(err)

	book := &models.ContactBook{}
	err = proto.Unmarshal(f, book)
	Check(err)

	// Если файл пустой - выходим
	if IsFileEmpty(len(book.Contacts)) {
		return
	}

	// Перебираем все контакты из файла
	for _, contact := range book.Contacts {
		// Если текущий номер (из файла) совпал с введенным - выводим инфу о нем на дисплей
		if contact.Phone == number {
			fmt.Println("Найдена запись:")
			// Отображаем контакт через форматированный вывод
			PrintContact(contact)
			return
		}
	}
	fmt.Println("Запись с таким номером не существует")
}

func EditContact(contact *models.Contact) bool {
	f, err := ioutil.ReadFile(FileName)
	Check(err)

	book := &models.ContactBook{}
	err = proto.Unmarshal(f, book)
	Check(err)

	// Если файл пустой - выходим
	if IsFileEmpty(len(book.Contacts)) {
		return false
	}

	// Перебираем все контакты из файла
	for idx, item := range book.Contacts {

		// Если текущий номер (из файла) совпал с новым (введенным) - входим в блок для замены информации по текущему номеру
		if item.Phone == contact.Phone {
			tmp := item
			book.Contacts[idx] = contact
			// Вписываем в найденный контакт новые данные
			output, err := proto.Marshal(book)
			// Записываем все в файл
			err = ioutil.WriteFile(FileName, []byte(output), 0644)
			Check(err)
			fmt.Println("Найдена запись:")
			PrintContact(tmp)
			fmt.Println("Изменена на:")
			PrintContact(book.Contacts[idx])
			return true
		}
	}
	fmt.Println("Запись с таким номером не существует")
	return false
}

func DeleteContact(number string) bool {
	f, err := ioutil.ReadFile(FileName)
	Check(err)
	//rows := strings.Split(string(f), "\n")
	book := &models.ContactBook{}
	err = proto.Unmarshal(f, book)
	Check(err)

	// Если файл пустой - выходим
	if IsFileEmpty(len(book.Contacts)) {
		return false
	}

	//var rowItem []string
	//var changedRows []string
	var changedContacts []*models.Contact

	// Перебираем все строки из файла
	for idx, item := range book.Contacts {
		// Разбиваем каждую строку на поля
		//rowItem = strings.Split(item, ":")

		// Если текущий номер (из файла) совпал с введенным - выводим инфу о нем на дисплей и удаляем
		//if strings.EqualFold(rowItem[0], number) {
		if item.Phone == number {
			fmt.Println("Найдена запись:")
			PrintContact(item)
			// Делаем срез массива от начального элемента до текущего
			//changedRows = append(changedRows, rows[:idx]...)
			changedContacts = append(changedContacts, book.Contacts[:idx]...)
			// Увеличиваем индекс на 1 для исключения текущего значения
			idx++
			// Добавляем в срез массива данные от текущего+1 элемента до последнего элемента
			//changedRows = append(changedRows, rows[idx:]...)
			changedContacts = append(changedContacts, book.Contacts[idx:]...)
			book.Contacts = changedContacts
			//output := strings.Join(changedRows, "\n")

			output, err := proto.Marshal(book)
			Check(err)
			// Пишем все в файл
			err = ioutil.WriteFile(FileName, []byte(output), 0644)
			Check(err)
			return true
		}
	}

	fmt.Println("Запись с таким номером не существует")
	return false
}

func PrintContacts(contacts *models.ContactBook) {
	for _, contact := range contacts.Contacts {
		PrintContact(contact)
	}
}

func PrintContact(contact *models.Contact) {
	fmt.Println("contact {")
	fmt.Println("NAME:", contact.Name)
	fmt.Println("PHONE:", contact.Phone)
	fmt.Println("ADDRESS:", contact.Address)
	fmt.Println("}")
}

func CheckParamsExceptApartment(phone, name, city, street, building string) bool { // Если аргументы не проходят валидацию (все, кроме квартиры, т.к. она опциональная), выводим инфу об этом
	if Validate(phone, PhoneFlag) && Validate(name, NameFlag) && Validate(city, CityFlag) && Validate(street, StreetFlag) && Validate(building, BuildingOrApartmentFlag) {
		return true
	}
	PrintValidationMessages()
	return false
}

func IsFileEmpty(length int) bool {
	if length < 1 {
		fmt.Println("В файле нет записей")
		return true
	}
	return false
}

func IsFileExist() bool {
	if _, err := os.Stat(FileName); os.IsNotExist(err) {
		fmt.Println("Файл с номерами пока отсутствует. Для его создания совершите добавление записи")
		return false
	}
	return true
}

func Validate(data string, flag string) bool {
	switch flag {
	case PhoneFlag:
		return PhoneRegexp.MatchString(data)
	case NameFlag:
		return NameRegexp.MatchString(data)
	case CityFlag:
		return CityRegexp.MatchString(data)
	case StreetFlag:
		return StreetRegexp.MatchString(data)
	case BuildingOrApartmentFlag:
		return BuildingOrApartmentRegexp.MatchString(data)
	default:
		return false
	}
	//return false
}

func PrintValidationMessages() string {
	fmt.Println("Попробуйте еще раз. При этом введите верные данные")
	fmt.Println("Формат номера телефона +380xxYYYYYYY")
	fmt.Println("Имя - только буквы A-z и/или цифры (от 3 до 15 символов)")
	fmt.Println("Город - только буквы A-z (от 3 до 15 символов)")
	fmt.Println("Улица - только буквы A-z и/или цифры (от 4 до 20 символов)")
	fmt.Println("Дом/квартира - только буквы A-z и/или цифры (от 1 до 5 символов)")
	return "PrintValidationMessages()"
}

func Check(e error) {
	if e != nil {
		panic(e)
	}
}
